<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Games;
use App\Entity\Categories;
use App\Entity\Articles;
use App\Entity\Contact;
use App\Entity\Quizz;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class AppController extends AbstractController
{
    /**
     * @Route("/app", name="app")
     */
    public function accueil()
    {
        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository(Games::class)->findBy([],['date' => 'desc'],4);
        $articles = $em->getRepository(Articles::class)->findBy([],['id' => 'desc'],3);
        return $this->render('app/accueil.html.twig', [
            'games' => $games, 'articles' => $articles
        ]);
    }
    public function actualites()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Articles::class)->findAll();
        return $this->render('app/actualites.html.twig', [
            'articles' => $articles
        ]);
    }
    public function actualitesDetails($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Articles::class)->findOneBy(['id'=> $id]);
        return $this->render('app/actualitesDetails.html.twig', [
            'article' => $article
        ]);
    }
    public function classement()
    {
        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository(Games::class)->findBy([],['id' => 'asc']);
        $categories = $em->getRepository(Categories::class)->findAll();
        return $this->render('app/classement.html.twig', [
            'games' => $games, 'categories' => $categories
        ]);
    }
    public function classementDetails($id)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository(Games::class)->findOneBy(['id'=> $id]);
        return $this->render('app/classementDetails.html.twig', [
            'game' => $game
        ]);
    }
    public function contact(MailerInterface $mailer,Request $request)
    {
        $contact =New Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $emaildata = $form->getData();
            $email = (new Email())
                ->from($emaildata->getEmail())
                ->to('Contact@lovingVideoGames.com')
                ->subject($emaildata->getName())
                ->html($emaildata->getMessage());

            $mailer->send($email);
            $this->addFlash('success', 'Votre message à bien été envoyé !');
           return $this->redirect($request->getUri());
        
        }
        
        return $this->render('app/contact.html.twig',[
            'form' => $form->createView()
        ]);
    }

    public function quiz()
    {
        $em = $this->getDoctrine()->getManager();
        $questions = $em->getRepository(Quizz::class)->findRand();
        return $this->render('app/quiz.html.twig', [
            'questions' => $questions
        ]);
    }
    
    public function cgu()
    {
        return $this->render('app/cgu.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
}
