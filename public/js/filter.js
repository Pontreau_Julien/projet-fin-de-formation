$('input').each(function(){
    $(this).on("click", function(){
        var classe = $(this).attr('id')
        if( $(this).is(':checked') ){
            $('.' + classe).removeClass('hide').addClass('show');
        } else {
            $('.' + classe).removeClass('show').addClass('hide');
        } 
    });
});
